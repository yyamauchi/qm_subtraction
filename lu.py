import jax.numpy as jnp

def lu_mul(LU, x):
    N = LU.shape[0]
    L = jnp.tril(LU, k=-1)
    L = L.at[jnp.diag_indices(N)].set(1.)
    U = jnp.triu(LU)
    return L @ (U @ x)

def lu_mat(LU):
    N = LU.shape[0]
    L = jnp.tril(LU, k=-1)
    L = L.at[jnp.diag_indices(N)].set(1.)
    U = jnp.triu(LU)
    return L @ U

