#!/usr/bin/env python

import unittest

import jax
import jax.numpy as jnp

import flow
from lu import lu_mat,lu_mul

import free
from models import well

class TestFlowJacobians(unittest.TestCase):
    def _test_flow(self, f, N):
        kx = jax.random.PRNGKey(0)
        x = jax.random.normal(kx, (6,))
        _, ld = f(x)
        jac = jax.jacfwd(lambda x: f(x)[0])(x)
        phase, logdet = jnp.linalg.slogdet(jac)
        logdet = logdet + jnp.log(phase)
        self.assertAlmostEqual(ld, logdet)

    def test_LUFlow(self):
        self._test_flow(flow.LUFlow(jax.random.PRNGKey(0), 6, 2), 6)

    def test_RealNVP(self):
        self._test_flow(flow.RealNVP(jax.random.PRNGKey(0), 6, 2), 6)

class TestContourJacobians(unittest.TestCase):
    def _test_contour(self, c, N):
        kx = jax.random.PRNGKey(0)
        x = jax.random.normal(kx, (6,))
        # TODO

class TestModelAsymptotics(unittest.TestCase):
    def _test_asymptotic(self):
        pass

    def test_well(self):
        m = well.Model(
            m = 1.,
            V0 = 0.,
            nt = 20,
            dt = 0.1,
            x0 = -1.,
            p0 = 1.2,
            dx = 3.,
        )
        M = m.M
        b = m.b
        f = free.FreeFlow(M, b)
        k1, k2 = jax.random.split(jax.random.PRNGKey(0))
        x1 = jax.random.normal(k1, (m.D,))
        x2 = jax.random.normal(k2, (m.D,))
        y1 = f(x1)
        y2 = f(x2)
        S1 = m.action(y1)
        S2 = m.action(y2)
        dS1 = S1 - jnp.sum(x1*x1)/2.
        dS2 = S2 - jnp.sum(x2*x2)/2.
        self.assertAlmostEqual(dS1, dS2, places=5)

if __name__ == '__main__':
    unittest.main()

