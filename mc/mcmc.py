"""
Implementations of various Markov chain Monte Carlo methods.

A general note on design. Each Monte Carlo method here involves two classes;
one is the "control" class, and the other is only to carry state around. The
point of the second is that it's a valid jax PyTree, and so can be passed into
and out of JIT'd functions.
"""

from dataclasses import dataclass
from typing import Any

import equinox as eqx
import jax
import jax.numpy as jnp

class MetropolisChain(eqx.Module):
    x: jnp.array
    p: Any
    delta: Any
    key: Any
    history: jnp.array
    step: int

    def current(self):
        return self.x

class Metropolis:
    """

    """
    def __init__(self, action):
        self.action = action

    def init(self, x0, p, *, key, delta=1.):
        return MetropolisChain(
                x = x0,
                p = p,
                delta = delta,
                key = key,
                history = jnp.zeros(100),
                step = 0
        )

    def step(self, chain, N=1):
        def _step(i, chain, S):
            x = chain.x
            delta = chain.delta

            # Make proposal
            kprop, kacc, key = jax.random.split(chain.key, 3)
            xp = x + delta*jax.random.normal(kprop, x.shape) / jnp.sqrt(len(x))

            # Compute difference in actions
            Sp = self.action(xp, chain.p)
            acc = jax.random.uniform(kacc) < jnp.exp(S-Sp)

            # Accept or reject
            S = jax.lax.select(acc, Sp, S)
            x = jax.lax.select(acc, xp, x)

            H = len(chain.history)
            history = chain.history.at[i%H].set(acc)

            chain = MetropolisChain(
                    x = x,
                    p = chain.p,
                    delta = delta,
                    key = key,
                    history = history,
                    step = chain.step+1
            )
            return (chain, S)

        # Initial action
        S = self.action(chain.x, chain.p)
        # Run the chain
        chain_params, chain_static = eqx.partition(chain, eqx.is_array)
        def _step_filtered(i, dat):
            chain_params, S = dat
            chain = eqx.combine(chain_params, chain_static)
            chain, S = _step(i, chain, S)
            chain_params, _ = eqx.partition(chain, eqx.is_array)
            return chain_params, S
        chain_params, S = jax.lax.fori_loop(0, N, _step_filtered, (chain_params, S))
        return eqx.combine(chain_params, chain_static)

    def statistics(self, chain):
        return {
            'acceptance_rate': jnp.mean(chain.history),
        }

    def calibrate(self, chain):
        """
        Should not be called within a JIT'd function.
        """
        advance = eqx.filter_jit(lambda c: self.step(c, N=100))
        ar = self.statistics(chain)['acceptance_rate']
        lo, hi = 0.3, 0.55
        while ar < lo or ar > hi:
            delta = chain.delta
            scale = 1 - (ar < lo)*0.02 + (ar > hi)*0.02
            delta *= scale
            chain = eqx.tree_at(lambda c: c.delta, chain, delta)
            chain = advance(chain)
            ar = self.statistics(chain)['acceptance_rate']
            print(f'# Acceptance rate: {ar:.5f}, delta: {delta:.5f}')
        return chain

class Replicas(eqx.Module):
    x: jnp.array
    p: Any
    hbar: jnp.array
    delta: jnp.array
    key: Any
    history: jnp.array
    swaps: jnp.array
    step: int

    def current(self):
        return self.x[0,:]

class ReplicaExchange:
    def __init__(self, action):
        self.action = action

    def init(self, x0, p, R, hbar_max, *, key):
        r = jnp.arange(R)/R
        # hbar = r*(hbar_max-1) + 1 # Linear
        hbar = jnp.exp(r*jnp.log(hbar_max)) # Exponential
        return Replicas(
                x = jnp.tile(x0, (R,1)),
                p = p,
                hbar = hbar,
                delta = jnp.ones((R,)),
                key = key,
                history = jnp.zeros((R,100)),
                swaps = jnp.zeros(1000),
                step = 0
        )

    def statistics(self, replicas):
        return {
            'acceptance_rate': float(jnp.mean(replicas.history)),
            'swap_rate': float(jnp.mean(replicas.swaps > 0)),
            #'swap_bottom': float(jnp.mean(replicas.swaps == 1)),
        }

    def calibrate(self, replicas):
        advance = eqx.filter_jit(lambda c: self.step(c, N=100))
        replicas = advance(replicas)
        lo, hi = 0.3, 0.55
        ar = jnp.mean(replicas.history, axis=1)
        while jnp.any(ar < lo) or jnp.any(ar > hi):
            delta = replicas.delta
            scale = 1 - (ar < lo)*0.02 + (ar > hi)*0.02
            delta *= scale
            replicas = eqx.tree_at(lambda r: r.delta, replicas, delta)
            replicas = advance(replicas)
            ar = jnp.mean(replicas.history, axis=1)
            print(f'# Average acceptance rate: {jnp.mean(ar):.5f}, Average delta: {jnp.mean(delta):.5f}')
        return replicas

    def step(self, replicas, N=1):
        def _swap(S, x, hbar, key):
            R = x.shape[0]
            kprop, kacc = jax.random.split(key)
            i = jax.random.randint(kprop, (), 1, R)
            j = i-1
            diff = (S[i]-S[j])/hbar[i] + (S[j]-S[i])/hbar[j]
            swacc = jax.random.uniform(kacc) < jnp.exp(diff)
            idx = jnp.array([i,j])
            idxp = jnp.array([j,i])
            nidx = jax.lax.select(swacc, idxp, idx)
            S = S.at[idx].set(S[nidx])
            x = x.at[idx,:].set(x[nidx,:])
            return (S, x, jax.lax.select(swacc,i,0))

        def _step(replicas, S):
            # First we take a step on every chain; then we do a bit of mixing
            # of the chains.
            x = replicas.x
            R = x.shape[0]
            delta = replicas.delta
            hbar = replicas.hbar
            kprop, kacc, kswap, key = jax.random.split(replicas.key, 4)

            # Make proposals
            xp = x + delta.reshape((R,1))*jax.random.normal(kprop, x.shape) / jnp.sqrt(x.shape[1])

            # Compute differences in actions, and acceptance decisions
            Sp = jax.vmap(lambda y: self.action(y, replicas.p))(xp)
            acc = jax.random.uniform(kacc, shape=(R,)) < jnp.exp((S-Sp)/hbar)
            accx = jnp.tile(acc.reshape((R,1)), (1,x.shape[1]))

            # Accept or reject
            S = jax.lax.select(acc, Sp, S)
            x = jax.lax.select(accx, xp, x)
           
            # Update history
            H = replicas.history.shape[1]
            history = replicas.history.at[:,replicas.step%H].set(acc)

            # Swap chains and update log
            S, x, sw = _swap(S, x, hbar, kswap)
            H = len(replicas.swaps)
            swaps = replicas.swaps.at[replicas.step%H].set(sw)
            # TODO probably better to perform R swaps instead of just one

            replicas = Replicas(
                    x = x,
                    p = replicas.p,
                    hbar = hbar,
                    delta = delta,
                    key = key,
                    history = history,
                    swaps = swaps,
                    step = replicas.step+1
            )
            return (replicas, S)

        # Initial actions
        S = jax.vmap(lambda x: self.action(x, replicas.p))(replicas.x)
        # Run N steps
        replicas_params, replicas_static = eqx.partition(replicas, eqx.is_array_like)
        def _step_filtered(i, dat):
            replicas_params, S = dat
            replicas = eqx.combine(replicas_params, replicas_static)
            replicas, S = _step(replicas, S)
            replicas_params, _ = eqx.partition(replicas, eqx.is_array_like)
            return replicas_params, S
        replicas_params, S = jax.lax.fori_loop(0, N, _step_filtered, (replicas_params, S))
        return eqx.combine(replicas_params, replicas_static)

