from functools import partial

import jax
import jax.numpy as jnp

@jax.jit
def _split(k):
    return jax.random.split(k,3)

@partial(jax.jit, static_argnums=1)
def _normal(k, shape):
    return jax.random.normal(k, shape)

_uniform = jax.jit(jax.random.uniform)

class Chain:
    def __init__(self, action, x0, key, delta=0.3, temperature=1.):
        self.action = action
        self.x = x0
        self.S = self.action(self.x)
        self.delta = delta
        self.temperature = 1.
        self._key = key
        self._recent = [False]

        def _accrej(kacc, x, S, xp, Sp, temperature):
            Sdiff = Sp - S
            accepted = False
            acc = _uniform(kacc) < jnp.exp(-Sdiff/temperature)
            return jax.lax.cond(acc, lambda: (xp, Sp, True), lambda: (x, S, False))

        self._accrej = jax.jit(_accrej)

    def step(self, N=1):
        self.S = self.action(self.x).real
        for _ in range(N):
            kstep, kacc, self._key = _split(self._key)
            xp = self.x + self.delta*_normal(kstep, self.x.shape)
            Sp = self.action(xp).real
            self.x, self.S, accepted = self._accrej(kacc, self.x, self.S, xp, Sp, self.temperature)
            self._recent.append(accepted)
        self._recent = self._recent[-100:]

    def calibrate(self, *, cb=None):
        # Adjust delta.
        self.step(N=100)
        while self.acceptance_rate() < 0.3 or self.acceptance_rate() > 0.55:
            if cb is not None:
                cb(self.acceptance_rate())
            if self.acceptance_rate() < 0.3:
                self.delta *= 0.98
            if self.acceptance_rate() > 0.55:
                self.delta *= 1.02
            self.step(N=100)

    def acceptance_rate(self):
        return sum(self._recent) / len(self._recent)

    def iter(self, skip=1):
        while True:
            self.step(N=skip)
            yield self.x

