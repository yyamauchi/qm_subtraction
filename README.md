# Real-time quantum mechanics via contour deformation methods

## Papers
If using code, or code derived from it, it may be appropriate to cite one or more of the following papers.


## Requirements
The code in this repository requires a resonably up-to-date python3. The main packages used are `jax`, `equinox`, `optax`. The required packages are listed in detail in requirements.txt. To install all libraries using `venv`, do:
```
python -m venv env
. env/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

## Workflow
To start, generate a lattice model by creating a model file. Then, generate and train a contour for the model with `contour.py`. Finally, collect samples from the model distribution by `sample.py`.

Here is an example:
```
mkdir -p data/
./nf.py data/model.pickle data/contour.pickle -l 1 -w 1 -t -10
./sample.py data/model.pickle data/nf.pickle 1 -B 10 -S 1000
```

## Organization

The main scripts are:
- **`nf.py`**: Trains a normalizing flow
- **`sample.py`**: Samples from a normalizing flow

The directory structure is:

- **`models/`**:Model distributions
