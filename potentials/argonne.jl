### A Pluto.jl notebook ###
# v0.19.26

using Markdown
using InteractiveUtils

# ╔═╡ e1014376-f871-11ed-15d6-b9ffb5f51b68
md"""
# Argonne potential fitting

Fits and manipulations of the Argonne potentials.

## Sources
There are two papers describing the Argonne potentials. The first (Wiringa, Stoks, Schiavilla, from 1994) described the classic "AV18" potential. The second (Wiringa and Pieper, 2002) discusses a few simplified projections, all likely to be useful. The goal of this file is to fit every single one. Both papers are in the `papers/` directory of this repository.

The general strategy is described in [Kanwar et al (2023)](https://arxiv.org/pdf/2304.03229.pdf). We fit to Chebyshev polynomials, because those can be analytically continued nicely, enabling us to use the method of contour deformations.

## To-Do

* Literally everything
* Package this stuff up?
* Illinois three-body potentials (separately, I guess)

"""

# ╔═╡ 47c64a38-0a92-42f9-9a8e-54ac3e58ee1c


# ╔═╡ Cell order:
# ╠═e1014376-f871-11ed-15d6-b9ffb5f51b68
# ╠═47c64a38-0a92-42f9-9a8e-54ac3e58ee1c
