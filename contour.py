#!/usr/bin/env python

# training integration contour for QM path integral

from functools import partial
import sys
import time
from typing import Callable

import equinox as eqx
import jax
import jax.numpy as jnp
import numpy as np
import optax

from mc import metropolis
from models import gauss1d

# Specify to use CPUs
jax.config.update('jax_platform_name', 'cpu')

def Identity(x):
    return x

# Class linear transformation with arbitrary initialization of weight and bias
class Linear(eqx.Module):
    weight: jnp.array
    bias: jnp.array

    def __init__(self, in_size, out_size, initweight, initbias):
        shape_w = np.shape(initweight)
        shape_b = np.shape(initbias)
        if shape_w[0] != out_size or shape_w[1] != in_size or shape_b[0] != out_size:
            print('Input weight of bias does not match the dimention of the model')
            exit()
        self.weight = initweight
        self.bias = initbias

    def __call__(self, x):
        return self.weight @ x + self.bias

class MLP(eqx.Module):
    dim: int
    activation: Callable 
    final_activation: Callable
    layers: list

    def __init__(self, indim, outdim, depth, width, actf, final_actf, key):
        self.dim = indim
        self.activation = actf
        self.final_activation = final_actf
        W = indim * width
        keys = jax.random.split(key, depth)
        self.layers = []
        if depth == 0:
            self.layers.append(Linear(indim, outdim, jnp.zeros((outdim, indim)), jnp.zeros(outdim)))
        else:
            self.layers.append(Linear(indim, W, jax.random.normal(keys[0],(W,indim)), jnp.zeros(W)))
            for i in range(1,depth):
                self.layers.append(Linear(W, W, jax.random.normal(keys[i],(W,W)), jnp.zeros(W)))
            self.layers.append(Linear(W, outdim, jnp.zeros((outdim,W)), jnp.zeros(outdim)))

    def __call__(self, x):
        for layer in self.layers[:-1]:
            x = layer(x)/np.sqrt(self.dim)
            x = self.activation(x)
        x = self.final_activation(x)
        x = self.layers[-1](x)/np.sqrt(self.dim)
        return x

class Contour(eqx.Module):
    mlp: MLP
    asymptotic_r: Linear
    asymptotic_i: Linear

    def __init__(self, key, dim, depth, width):
        self.mlp = MLP(dim, 2*dim, depth, width, jax.nn.celu, jax.nn.sigmoid, key)
        self.asymptotic_r = Linear(dim, dim, jnp.eye(dim), jnp.zeros(dim))
        self.asymptotic_i = Linear(dim, dim, jnp.zeros((dim,dim)), jnp.zeros(dim))

    def contour(self, x):
        xasym = self.asymptotic_r(x) + 1j * self.asymptotic_i(x)
        y = self.mlp(x)
        return xasym + y[::2] + 1j*y[1::2]

    def __call__(self, x):
        z = self.contour(x)
        jac = jax.jacfwd(self.contour)(x)
        phase, logdet = jnp.linalg.slogdet(jac)
        return z, logdet + jnp.log(phase)


class AffineCoupling(eqx.Module):
    mask: jnp.array
    scale: MLP
    trans: Linear

    def __init__(self, key, D, mask=None):
        maskKey, scaleKey = jax.random.split(key)
        if mask is None:
            self.mask = jax.random.randint(maskKey, (D,), 0, 2)
        else:
            self.mask = mask
        self.scale = MLP(D, D, 1, 1, jnp.tanh, Identity, scaleKey)
        self.trans = Linear(D, D, jnp.zeros((D,D)), jnp.zeros(D))

    def map(self, x):
        return self(x)[0]

    # Returns the output and log of Jacobian
    def __call__(self, x):
        y = self.mask * x
        s = self.scale(y)
        t = self.trans(y)
        z = y + (1-self.mask) * (jnp.exp(s)*x + t)
        return z, jnp.sum((1-self.mask) * s)

    # Inverse of the map and its Jacobian
    def inv(self, x):
        y = self.mask * x
        s = self.scale(y)
        t = self.trans(y)
        z = y + (1-self.mask) * (x-t) * jnp.exp(-s)
        return z, jnp.sum(-(1-self.mask) * s)


class CheckeredAffines(eqx.Module):
    even: AffineCoupling
    odd: AffineCoupling

    def __init__(self, key, D):
        key_even, key_odd = jax.random.split(key)
        mask_even = jnp.arange(D)%2
        mask_odd = 1 - mask_even
        self.even = AffineCoupling(key_even, D, mask=mask_even)
        self.odd = AffineCoupling(key_odd, D, mask=mask_odd)

    def map(self, x):
        return self(x)[0]

    def __call__(self, x):
        x, ld_e = self.even(x)
        x, ld_o = self.odd(x)
        return x, ld_e + ld_o

    # Inverse of the map and its Jacobian
    def inv(self, x):
        x, ld_o = self.odd.inv(x)
        x, ld_e = self.even.inv(x)
        return x, ld_e + ld_o

class RealNVP(eqx.Module):
    depth: int
    layers: list

    def __init__(self, key, D, depth):
        self.depth = depth
        keys = jax.random.split(key, depth)
        self.layers = [CheckeredAffines(k, D) for k in keys]

    def map(self, x):
        return self(x)[0]

    def __call__(self, x):
        ld = 0
        for l in self.layers:
            x, lj = l(x)
            ld += lj
        return x, ld

    # Inverse of the map and its Jacobian
    def inv(self, x):
        ld = 0
        for i in range(self.depth):
            x, lj = self.layers[-i-1].inv(x)
            ld += lj
        return x, ld


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description="Train contour",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        fromfile_prefix_chars='@')
    parser.add_argument('model', type=str, help="model filename")
    parser.add_argument('contour', type=str, help="contour filename")
    parser.add_argument('-d', '--depth', type=int, default=0, help='number of (hidden) layers')
    parser.add_argument('-w', '--width', type=int, default=1, help='width (scaling)')
    parser.add_argument('--seed', type=int, default=0, help="random seed")
    parser.add_argument('--seed-time', action='store_true',
                        help="seed PRNG with current time")

    args = parser.parse_args()

    with open(args.model, 'rb') as f:
        model = eval(f.read())
    V = model.D

    seed = args.seed
    if args.seed_time:
        seed = time.time_ns()
    key = jax.random.PRNGKey(seed)
    contour_ikey, chain_ikey = jax.random.split(key, 2)

    contour = Contour(contour_ikey, V, args.depth, args.width)

    @partial(jnp.vectorize, signature='(i)->()', excluded={0})
    def action_eff(c, x):
        xt, detJ= c(x)
        return model.action(xt) - detJ

    @eqx.filter_jit
    def re_action_eff(c, x):
        xt, detJ= c(x)
        return (model.action(xt) - detJ).real

    @eqx.filter_jit
    def im_action_eff(c, x):
        xt, detJ= c(x)
        return (model.action(xt) - detJ).imag

    @eqx.filter_grad
    def neg_grad_re_action_eff(c, x):
        return -jnp.mean(action_eff(c, x).real)

    pkey, chain_key = jax.random.split(chain_ikey, 2)
    chain = metropolis.Chain(lambda x: re_action_eff(contour, x), jnp.zeros(V), chain_key)
        
    opt = optax.adam(1e-3)
    opt_state = opt.init(eqx.filter(contour, eqx.is_array))

    chain.step(N=100)
    x = np.random.normal(size=(V))

    try:
        phase = 0
        step = 0
        while phase.real < 1.0 or step < 1000:
            xs = []
            phases = []
            if np.mean(np.abs(chain.x)) > 3:
                pkey, chain_key = jax.random.split(pkey, 2)
                chain = metropolis.Chain(lambda x: re_action_eff(contour, x), jnp.zeros(V), chain_key)
                print('MCMC reset')

            for _ in range(100):
                chain.step(N=V*10)
                xs.append(chain.x)
            acts = action_eff(contour, jnp.array(xs))
            print(jnp.mean(jnp.exp(-1j*acts.imag)), jnp.mean(acts.real), chain.x)
            grad = neg_grad_re_action_eff(contour, jnp.array(xs))
            updates, opt_state = eqx.filter_jit(opt.update)(grad, opt_state)
            contour = eqx.filter_jit(eqx.apply_updates)(contour, updates)
            step += 1
    except KeyboardInterrupt:
        pass
    eqx.tree_serialise_leaves(args.contour, contour)



    


    
