#!/usr/bin/env python

from typing import Callable

import equinox as eqx
from equinox import nn
import jax
import jax.numpy as jnp

from lu import lu_mat,lu_mul

def leaky_tanh(x):
    return 0.9*jnp.tanh(x) + 0.1*x

def zero_mlp(mlp):
    def where(mlp):
        return [mlp.layers[-1].weight, mlp.layers[-1].bias]
    return eqx.tree_at(where, mlp, replace_fn = jnp.zeros_like)

class LULinear(eqx.Module):
    lu: jnp.array

    def __init__(self, key, N):
        scale = 1/jnp.sqrt(N)
        lu = jnp.eye(N)
        self.lu = lu+jax.random.uniform(key, (N,N), minval=scale, maxval=scale)
        self.lu = lu

    def __call__(self, x):
        x = lu_mul(self.lu, x)
        ld = jnp.sum(jnp.log(jnp.diagonal(self.lu)))
        return x, ld

class LUFlow(eqx.Module):
    layers: list
    activation: Callable

    def __init__(self, key, N, depth, activation=leaky_tanh):
        keys = jax.random.split(key, depth+1)
        self.layers = [LULinear(k, N) for k in keys]
        self.activation = activation

    def __call__(self, x):
        ld = 0.
        for l in self.layers[:-1]:
            x, ld_ = l(x)
            ld += ld_
            x, g = jax.vmap(jax.value_and_grad(self.activation))(x)
            ld += jnp.sum(jnp.log(g))
        x, ld_ = self.layers[-1](x)
        ld += ld_
        return x, ld

class AffineCoupling(eqx.Module):
    mask: jnp.array
    scale: nn.MLP
    trans: nn.MLP

    def __init__(self, key, N, mask=None):
        if mask is None:
            key, key_mask = jax.random.split(key)
            self.mask = jax.random.randint(key, (N,), 0, 2)
        else:
            self.mask = mask
        key_scale, key_trans = jax.random.split(key)
        scale = nn.MLP(
                in_size=N,
                out_size=N,
                width_size=N,
                depth=1,
                key=key_scale,
                activation=jnp.tanh)
        trans = nn.MLP(in_size=N,
                       out_size=N,
                       width_size=N,
                       depth=0,
                       key=key_scale)
        self.scale = zero_mlp(scale)
        self.trans = zero_mlp(trans)

    def __call__(self, x):
        y = self.mask * x
        s = self.scale(y)
        t = self.trans(y)
        z = y + (1-self.mask) * (jnp.exp(s)*x + t)
        return z, jnp.sum((1-self.mask)*s)

class RealNVP(eqx.Module):
    layers: list

    def __init__(self, key, N, depth):
        keys = jax.random.split(key, depth)
        self.layers = [AffineCoupling(k, N) for k in keys]

    def __call__(self, x):
        ld = 0.
        for l in self.layers:
            x, ld_ = l(x)
            ld += ld_
        return x, ld

class CompositeFlow(eqx.Module):
    parts: list

    def __init__(self, *parts):
        self.parts = parts

    def __call__(self, x):
        ld = 0
        for p in self.parts:
            x, ld_ = p(x)
            ld += ld_
        return x, ld



if __name__ == '__main__':
    pass
