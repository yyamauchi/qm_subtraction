#!/usr/bin/env python

# QM path integral via free theory contour

"""
TODO

 * At moderate coupling (V0~0.3), there's occasionally an enormous reweighting
 from the normalizing flow. What causes this? (From metropolis: there's an unstable direction)

 * Clean up mc.metropolis, and optimize
"""

import argparse
from functools import partial
import sys
import time
from typing import Callable

import jax
import jax.numpy as jnp
import numpy as np
import optax

from mc import metropolis
from models import well

class FreeFlow:
    def __init__(self, M, b):
        w, P = np.linalg.eig(M)
        C = np.diag(w**(-0.5))
        D = np.diag(w**(-1))
        # M is: P @ np.diag(w) @ np.linalg.inv(P)
        self.M = M
        self.b = jnp.array(b)
        self.A = jnp.array(P @ C @ np.linalg.inv(P))
        self.B = jnp.array(P @ D @ np.linalg.inv(P))

    def __call__(self, x):
        return jnp.einsum('ij,j->i', self.A, x) - jnp.einsum('ij,j->i', self.B, self.b)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
            description="Free theory contour",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            fromfile_prefix_chars='@')
    parser.add_argument('--nf', action='store_true',
            help='Perform a full metropolis')
    parser.add_argument('model', type=str, help="model filename")

    args = parser.parse_args()

    with open(args.model, 'rb') as f:
        model = eval(f.read())
    V = model.D

    seed = time.time_ns()
    key = jax.random.PRNGKey(seed)

    flow = FreeFlow(model.M, model.b)

    @jax.jit
    def action_eff(x):
        return model.action(flow(x))

    if True:
        # Playing with asymptotics
        Z = flow.A.T @ flow.A
        print(flow.A)
        #print(np.linalg.eig(Z)[0])
        #x = jnp.array([-6., 21., -6])
        x = jnp.array([-6., 20., 0])
        y = flow(x)
        print(y)
        print(model.action_potential(flow(x)))
        exit()

    if args.nf:
        x = jnp.array(np.random.normal(size=(10000,V)))
        Seff = jax.vmap(action_eff)(x)
        Ssamp = jnp.einsum('ki,ki->k', x, x)/2.
        rew = jnp.exp(Ssamp-Seff.real)
        phase = jnp.exp(-1j*Seff.imag)
        print(jnp.mean(phase*rew) / jnp.mean(rew))
        print(jnp.mean(rew*rew)/jnp.mean(rew))
    else:
        chain = metropolis.Chain(lambda x:action_eff(x).real, jnp.zeros(V), key)
        chain.calibrate()

        xs = []
        for _ in range(100):
            chain.step(V*10)
            xs.append(chain.x)
            print(action_eff(chain.x))
        xs = jnp.array(xs)
        Ss = jax.vmap(action_eff)(xs)
        print(jnp.mean(jnp.exp(-1j * Ss.imag)))

