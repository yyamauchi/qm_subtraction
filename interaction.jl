#!/usr/bin/env julia

# Monte Carlo in the interaction picture

# Parameters of the model.
x₀ = -4
p₀ = 1

# The Fourier transform of the potential.
V(k) = exp(-k^2 / 2.)

# Number of domains.
K = 5

function weight(p, t)
end

# Initial configuration.

