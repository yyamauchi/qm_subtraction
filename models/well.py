from dataclasses import dataclass

import jax.numpy as jnp
import numpy as np

@dataclass
class Model:
    m: float
    V0: float
    nt: int
    dt: float
    x0: float
    p0: float
    dx: float

    def __post_init__(self):
        self.T = jnp.concatenate([-1j*jnp.ones(self.nt), 1j*jnp.ones(self.nt)]) * self.dt
        self.D = self.nt * 2 + 1
        self.M = np.zeros((self.D, self.D))*1j
        for i in range(self.D):
            if i < self.D-1:
                self.M[i,i] += 1./self.m/self.T[i]
                self.M[i,i+1] -= 1./self.m/self.T[i]
                self.M[i+1,i] -= 1./self.m/self.T[i]
            if i > 0:
                self.M[i,i] += 1./self.m/self.T[i-1]

        self.M[0,0] += 1./self.dx**2
        self.M[self.D-1,self.D-1] += 1./self.dx**2
        
        self.b = np.zeros(self.D)*1j
        self.b[0] = -self.x0/self.dx**2- 1j * self.p0
        self.b[self.D-1] = -self.x0/self.dx**2 + 1j * self.p0

    def initstate(self, x):
        return (x-self.x0)**2/2/self.dx**2 - 1j * self.p0 * x 

    def finstate(self, x):
        return (x-self.x0)**2/2/self.dx**2 + 1j * self.p0 * x

    def action_kinetic(self, x):
        kin = jnp.sum((x[1:] - x[:-1])**2/self.T)/2/self.m
        return kin

    def V(self, x):
        return - self.V0 * jnp.exp(-x**2)

    def action_potential(self, x):
        pot = jnp.sum(self.V(x[:-1])*self.T/2. + self.V(x[1:])*self.T/2.)
        return pot

    def action(self, x):
        return self.action_kinetic(x) + self.action_potential(x) + self.initstate(x[0]) + self.finstate(x[-1])

