from dataclasses import dataclass

import jax.numpy as jnp
import numpy as np

@dataclass
class Model:
    """
    A single spin passing through a (space-dependent) magnetic field.

    Parameters:

     - `m`: mass of particle

     - `mu`: strength of magnetic field
     - `V0`: strength of spin-independent potential
     - `L`: width of potential

     - nt: number of time steps
     - dt: size of time step

     - x0: initial position
     - p0: initial momentum
     - dx: position uncertainty
    """
    m: float
    mu: float
    V0: float
    L: float
    nt: int
    dt: float
    x0: float
    p0: float
    dx: float

    def __post_init__(self):
        pass

    def initstate(self, x, s):
        pass

    def action_kinetic(self, x, s):
        pass

    def V(self, x, s):
        well = jnp.exp(-x**2/(2.*self.L**2))
        return (self.V0 + (1-s)*self.mu)*well

    def action_potential(self, x, s):
        pass

    def action(self, x, s):
        pass

