#!/usr/bin/env python

"""
Simulation of scattering via holomorphic gradient flow.
"""

import argparse
import time

import equinox as eqx
from diffrax import diffeqsolve, ODETerm, Dopri5, DiscreteTerminatingEvent, DirectAdjoint
import jax
import jax.numpy as jnp

#TODO
jax.config.update("jax_enable_x64", True)

from mc import mcmc, metropolis
from models import well, resonance

if __name__ == '__main__':
    # Specify to use CPUs
    jax.config.update('jax_platform_name', 'cpu')

    parser = argparse.ArgumentParser(
            description="Simulate using holomorphic gradient flow",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            fromfile_prefix_chars='@')
    parser.add_argument('-T', '--flow-time', type=float, default=1e-2,
            help='Amount of HGF evolution to perform.')
    parser.add_argument('-S', '--nsamples', type=int, default=10000,
            help='Number of samples in MCMC')
    parser.add_argument('model', type=str, help="model filename")
    parser.add_argument('-R', '--nreplicas', type=int, default=10, help='Number of replicas')
    parser.add_argument('-M', '--maxbeta', type=float, default=100., help='Maximal beta in replica')
    parser.add_argument('--seed', type=int, default=1, help='Random seed')
    parser.add_argument('--hessian', action='store_true', help='Use hessian in MCMC')
    parser.add_argument('--threshold', type=int, default=100, help='Use hessian in MCMC')
    parser.add_argument('--replica', action='store_true', help='Use replica')


    args = parser.parse_args()

    with open(args.model, 'rb') as f:
        model = eval(f.read())

    #seed = time.time_ns()
    seed = args.seed
    key = jax.random.PRNGKey(seed)

    def hgf(t, y, args):
        z = y[:model.D] + 1j*y[model.D:]
        dz = jax.grad(model.action, holomorphic=True)(z).conj()
        return jnp.concatenate([dz.real, dz.imag])

    hgf_term = ODETerm(hgf)
    hgf_solver = Dopri5()
            #scan_kind="bounded")

    # TODO
    event_thr = args.threshold

    def event(state, **kwargs):
        z = state.y[:model.D] + 1j*state.y[model.D:]
        s = model.action(z)
        return (s.real > event_thr)

    def flow_(x):
        y = jnp.concatenate([x,jnp.zeros(model.D)])
        # TODO adaptive step size
        #sol = diffeqsolve(hgf_term, hgf_solver, t0=0, t1=args.flow_time, dt0=1e-3, max_steps=10000, y0=y)
        sol = diffeqsolve(hgf_term, hgf_solver, t0=0, t1=args.flow_time, dt0=1e-3, max_steps=10000, y0=y, discrete_terminating_event= DiscreteTerminatingEvent(event))
        return sol.ys[-1]


    @jax.jit
    def flow(x):
        y = flow_(x)
        z = y[:model.D] + 1j*y[model.D:]
        return z

    @jax.jit
    def action_eff(x):
        z = flow(x)
        dydx = jax.jacrev(flow_)(x)
        jac = dydx[:model.D,:] + 1j*dydx[model.D:,:]
        s, logdet = jnp.linalg.slogdet(jac)
        return model.action(z) - logdet - jnp.log(s), model.action(z), logdet, jnp.log(s)

    def action_eff_real(x, p):
        a, b, c, d = action_eff(x)
        return a.real

    def flow_hessian(x):
        y = jnp.concatenate([x,jnp.zeros(model.D)])
        sol = diffeqsolve(hgf_term, hgf_solver, t0=0, t1=args.flow_time, dt0=1e-3, max_steps=100, y0=y, adjoint=DirectAdjoint())
        return sol.ys[-1] 

    # Hessian
    @jax.jit
    @jax.hessian
    def hessian(x):
        sol = flow_hessian(x)
        z = sol[:model.D] + 1j*sol[model.D:]
#        dydx = jax.jacrev(flow_hessian)(x)
#        jac = dydx[:model.D,:] + 1j*dydx[model.D:,:]
#        s, logdet = jnp.linalg.slogdet(jac)
        return model.action(z).real

    hes = jnp.eye(model.D)
    if args.hessian:
        hes = hessian(jnp.zeros(model.D))
    val, vec = jnp.linalg.eigh(hes)
    scale = vec @ jnp.sqrt(jnp.diag(1/val))
    #print(f'# {val}')
    #print(scale.T @ hes @ scale) Check that scale makes hessian I

    # Make MCMC chain
    if args.replica:
        mc = mcmc.ReplicaExchange(lambda z,p: action_eff_real(scale@z, p))
        chain = mc.init(jnp.zeros(model.D), 1, args.nreplicas, args.maxbeta, key=key)
    else:
        mc = mcmc.Metropolis(lambda z,p: action_eff_real(scale@z, p))
        chain = mc.init(jnp.zeros(model.D), 1, key=key, delta=1./jnp.sqrt(model.D))

    @eqx.filter_jit
    def step(chain, x):
        chain_params, chain_static = eqx.partition(chain, eqx.is_array_like)
        chain = mc.step(chain, N=model.D*10)
        chain_params, _ = eqx.partition(chain, eqx.is_array_like)
        chain = eqx.combine(chain_params, chain_static)
        return chain, chain.current()


    # Thermalization
    for _ in range(10):
        chain, x = step(chain, chain.current())

    chain = mc.calibrate(chain)

    try:
        nsamples = 0
        while nsamples < args.nsamples:
            chain, x = step(chain, chain.current())
            x = scale @ x
            S, act, logdet, sign = action_eff(x)
            print(jnp.exp(-1j*S.imag), S, jnp.mean(jnp.abs(chain.x)), act, logdet, sign, flush=True)
            nsamples += 1
    except None:
        pass


